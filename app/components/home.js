var serverLocation = "http://localhost:8080/api/";
var getSalesInfoPath = serverLocation + '/reportByDate/';
var getTopSalesManInfoPath = serverLocation + '/reportBySalesMan/';
var getBillWiseInfo = serverLocation + '/invoiceByDate'
var getBillInfo = serverLocation + 'invoiceInfo/'
var getItemWiseInfo = serverLocation+ 'reportByProduct/'
var getItemInfo = serverLocation+ 'productInfo/'
var getPurchaseInfoByDate = serverLocation+ 'purchaseByDate/'

var backEndDateFormat = 'YYYY-MM-DD'

import React, { Component } from 'react'
import Appbar from './Appbar'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import axios from 'axios'
import _ from 'lodash'

export default class Home extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      backEndDateFormat: 'YYYY-MM-DD',
      dispalyDateFormate: "DD-MM-YYYY",
      todaySalesData: {},
      salesManChartData: {},
      salesInfoChartData: {},
      todaySalesFrom: moment(),
      todaySalesTo: moment(),
      salesInfoFrom: moment().subtract(3, 'days'),
      salesInfoTo: moment(),
      salesManInfoFrom: moment().subtract(30, 'days'),
      salesManInfoTo: moment()
    }
    this.changeSalesFromDate = this.changeSalesFromDate.bind(this)
    this.changeSalesToDate = this.changeSalesToDate.bind(this)
    this.changeSalesManFromDate = this.changeSalesManFromDate.bind(this)
    this.changeSalesManToDate = this.changeSalesManToDate.bind(this)

    this.constructSalesInfoChart = this.constructSalesInfoChart.bind(this)
    this.constructSalesManInfoChart = this.constructSalesManInfoChart.bind(this)
    this.constructTodaySalesChart = this.constructTodaySalesChart.bind(this)
  }
  changeSalesFromDate(date) {
    this.setState({
      salesInfoFrom: date
    })
  }
  changeSalesToDate(date) {
    this.setState({
      salesToFrom: date
    })
  }
  changeSalesManFromDate(date) {
    this.setState({
      salesManInfoFrom: date
    })
  }
  changeSalesManToDate(date) {
    this.setState({
      salesManInfoTo: date
    })
  }
  constructSalesInfoChart(){
    var state = this.state;
    var fromDate = moment(state.salesInfoFrom).format(backEndDateFormat)
      var toDate = moment(state.salesInfoTo).format(backEndDateFormat)
      axios.get(getSalesInfoPath, {
        params: {fromDate: fromDate, toDate: toDate}
      })
      .then(function (response) {
        console.log(response);
        this.renderSalesInfoChart(response.data);
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
  }
  renderSalesInfoChart(salesInfo){
      var state = this.state;
      var salesDates = _.map(salesInfo, 'createdDate')
  		var salesTotalAmount = _.map(salesInfo, 'totalAmount')

  		 var salesChartData = {
          "type": "line", // Specify your chart type here.
          "utc": true,
          /* Set to UTC time. */
          "scale-x": {
                "min-value": new Date(state.salesInfoFrom).getTime(),
  			         "max-value": new Date(state.salesInfoTo).getTime(),
                 "step": "day",
                  "transform": {
                      "type": "date",
                      "all": "%M %d, %Y"
                  }
            },
          "series": [ // Insert your series data here.
              {
                  "values": salesTotalAmount,
                  backgroundColor: "#ea5443",
                  alpha: 0.8,
                  marker: {
                      "background-color": "#FF0066",
                      "size": 5
                  },
              }
          ]
      };
      zingchart.render({ // Render Method
          id: 'sales-chart',
          data: salesChartData,
          height: 300,
          width: "100%"
      });
  }

  constructSalesManInfoChart(){
    var state = this.state;
    var fromDate = moment(state.salesManInfoFrom).format(backEndDateFormat)
      var toDate = moment(state.salesManInfoTo).format(backEndDateFormat)
      axios.get(getTopSalesManInfoPath, {
        params: {fromDate: fromDate, toDate: toDate}
      })
      .then(function (response) {
        console.log(response);
        this.renderSalesManChart(response.data);
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
  }
  renderSalesManChart(salesManInfo) {
  		var salesManNames = _.map(salesManInfo, 'salesRep')
  		var salesManTotalAmount = _.map(salesManInfo, 'totalAmount')

          var salesManChartData = {
              "type": "bar", // Specify your chart type here.
              "scale-x": {
                  "label": {
                      "text": "Sales Persons"
                  },
                  "labels": salesManNames
              },
              "series": [ // Insert your series data here.
                  {
                      "values": salesManTotalAmount,
                      backgroundColor: "#fc5f29",
                      alpha: 0.8
                  }
              ]
          };
          zingchart.render({ // Render Method
              id: 'sales-man-chart',
              data: salesManChartData,
              height: 300,
              width: "100%"
          });
  }
  constructTodaySalesChart(){
    var state = this.state;
    var fromDate = moment(state.todaySalesFrom).format(backEndDateFormat)
      var toDate = moment(state.todaySalesFrom).format(backEndDateFormat)
      axios.get(getSalesInfoPath, {
        params: {fromDate: fromDate, toDate: toDate}
      })
      .then(function (response) {
        console.log(response);
        this.renderTodaySalesChart(response.data);
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
  }
  renderTodaySalesChart(data){
      console.log(data)
      var totalAmount = _.isUndefined(data[0]) ? 0 : data[0].totalAmount
      var todaySalesData = {
          "type": "gauge",
          "series": [{
              "values": [totalAmount]
          }],
          "scale-r": {
              "ring": {
                  "size": 10,
                  "background-color": "purple"
              }
          }
      };
      zingchart.render({ // Render Method
          id: 'today-sales-chart',
          data: todaySalesData,
          height: 300,
          width: "100%"
      });

    }
  render() {
    this.constructSalesInfoChart()
    this.constructSalesManInfoChart()
    this.constructTodaySalesChart()
    var state = this.state;
    var props = this.props;
    var dispalyDateFormate = state.dispalyDateFormate;
    return (
      <div>
        <Appbar activeTab = {props.route.activeTab}/>
        <div className="container-fluid top30">
           <div className="row">
              <div className="col s12">
                 <h3>Dashboard</h3>
              </div>
           </div>
           <div className="row">
            <div className="col s4">
                 <div className="card">
                    <div className="card-content white-text">
                       <span className="card-title grey-text text-darken-4">Today's sales</span>
                       <div id="today-sales-chart"></div>
                    </div>
                 </div>
              </div>

              <div className="col s4">
                <h5>Sales Info</h5>
                <div className="col s6">
                  <div>From</div>
                  <DatePicker
                    dateFormat={dispalyDateFormate}
                    selected={state.salesInfoFrom}
                    onChange={this.changeSalesFromDate} />
                </div>
                <div className="col s6">
                  <div>To</div>
                  <DatePicker
                    dateFormat={dispalyDateFormate}
                    selected={state.salesInfoTo}
                    onChange={this.changeSalesToDate} />
                </div>

                <div className="row">
                  <div className="col s12">
                    <div id="sales-chart"></div>
                  </div>
                </div>
              </div>

              <div className="col s4">

               <h5>Top Sales Man</h5>
                 <div className="col s6">
                   <div>From</div>
                   <DatePicker
                     dateFormat={dispalyDateFormate}
                     selected={state.salesManInfoFrom}
                     onChange={this.changeSalesManFromDate} />
                 </div>
                 <div className="col s6">
                   <div>To</div>
                   <DatePicker
                     dateFormat={dispalyDateFormate}
                     selected={state.salesManInfoTo}
                     onChange={this.changeSalesManToDate} />
                 </div>
             <div className="row">
              <div className="col s12">
                <div id="sales-man-chart"></div>
              </div>
             </div>

              </div>
           </div>
        </div>
      </div>

    )
  }
}
