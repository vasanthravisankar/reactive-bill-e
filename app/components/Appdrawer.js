
import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';

export default class Appdrawer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {open: false};
  }
  toggleMenu(){
    this.props.toggleMenu()
  }
  render() {
    return (
      <div>
        <Drawer
          iconElementLeft = {<FlatButton label="" />}
          onLeftIconButtonTouchTap={this.toggleMenu}
          docked={false} open={this.props.open}
          onRequestChange={this.toggleMenu}
          >
          <MenuItem>Menu Item</MenuItem>
          <MenuItem>Menu Item 2</MenuItem>
        </Drawer>
      </div>
    );
  }
}
