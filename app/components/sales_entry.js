var serverLocation = "http://localhost:8080/api/";
var productInfoUrl = serverLocation + 'productInfo/'
var vendorInfoUrl = serverLocation + 'vendorInfo/'
var salesManInfoUrl = serverLocation + 'salesManInfo/'
var salesInfoUrl = serverLocation + 'invoiceInfo/'

var backEndDateFormat = 'YYYY-MM-DD'

import React from 'react';
import Appbar from './Appbar'
import Dialog from 'material-ui/Dialog';
import DatePicker from 'react-datepicker'
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import Snackbar from 'material-ui/Snackbar';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import ReactDataGrid from 'react-data-grid';
import moment from 'moment'
import axios from 'axios'
import _ from 'lodash'

import Select from 'react-select';

var styles = {
  floatingLabelFocusStyle: {
    'fontSize': '18px'
  },
  txtBox: {
      'width': '100%'
  },
  td: {
      'padding': '5px'
  },
  btnStyle: {
    'margin': '12px'
  }
}

var batchListTableColumns =   [

            {
              key: 'batchNo',
              name: 'Batch',
              width: 100
            },
            {
              key: 'expiryDate',
              name: 'Exp. Date',
              width: 70
            },
            {
              key: 'stock',
              name: 'Stock',
              width: 70
            },
            {
              key: 'quantity',
              name: 'Qty',
              editable: true,
              width: 70
            },
            {
              key: 'discountPercentage',
              name: 'Disc %',
              editable: true,
              width: 60
            },
            {
              key: 'sellingRate',
              name: 'S. Rate',
              width: 60
            },
            {
              key: 'mrp',
              name: 'MRP',
              width: 60
            },

    ]
var salesTableColumns =   [
            {
              key: 'name',
              name: 'Name',
              width: 120
            },
            {
              key: 'batchNo',
              name: 'Batch',
              editable: true,
              width: 100
            },
            {
              key: 'expiryDate',
              name: 'Exp. Date',
              editable: true,
              width: 70
            },
            {
              key: 'purchaseRate',
              name: 'P. Rate',
              width: 60
            },
            {
              key: 'mrp',
              name: 'MRP',
              width: 60
            },
            {
              key: 'quantity',
              name: 'Qty',
              editable: true,
              width: 70
            },
            {
              key: 'discountPercentage',
              name: 'Disc %',
              editable: true,
              width: 60
            },
            {
              key: 'sellingRate',
              name: 'S. Rate',
              width: 60
            },
            {
              key: 'productTotal',
              name: 'Total',
              width: 100
            },
            {
              key: 'actions',
              name: 'Actions',
              width: 100
            },

    ]

var sampleSales = {
    netAmount: 0,
    discountpercent: 0,
    entryNo: '',
    createdDate: moment(),
    salesMan: {},
    invoiceProductList: [],
    doctorName: '',
    cuetomerName: ''
    }
/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
 export default class salesEntry extends React.Component {

   constructor(props) {
     super(props);
     this.state = {
        productsModalOpen: false,
        backEndDateFormat: 'YYYY-MM-DD',
        dispalyDateFormate: "DD-MM-YYYY",
        selectedProduct: '',
        newSalesProduct: {},
        sales: _.clone(sampleSales),
        vendors: [],
        products: [],
        salesMans: [],
        productBatchList: [],
        individualQty: 0,

        update: false,
        autoFocus: false,
        statusMessageOpen: false,
        statusMessage: ''
      }

      this.handleTextChange = this.handleTextChange.bind(this)
      this.changeCreatedDate = this.changeCreatedDate.bind(this)
      this.changeSalesMan = this.changeSalesMan.bind(this)
      this.changeSelectedProduct = this.changeSelectedProduct.bind(this)
      this.changeOverallDiscount = this.changeOverallDiscount.bind(this)
      this.addProductToSales = this.addProductToSales.bind(this)
      this.setProductBatchList = this.setProductBatchList.bind(this)

      this.calculateSalesTotalPrice = this.calculateSalesTotalPrice.bind(this)
      this.rowGetter = this.rowGetter.bind(this)
      this.batchRowGetter = this.batchRowGetter.bind(this)
      this.handleGridRowsUpdated = this.handleGridRowsUpdated.bind(this)
      this.handleBatchdRowsUpdated = this.handleBatchdRowsUpdated.bind(this)
      this.changeQty = this.changeQty.bind(this)
      this.allotQuantity = this.allotQuantity.bind(this)
      this.reset = this.reset.bind(this)

      this.saveSales = this.saveSales.bind(this)

      this.getAllProducts = this.getAllProducts.bind(this)
      this.getPurchaseEntryForProduct = this.getPurchaseEntryForProduct.bind(this)

      this.openProductsModal = this.openProductsModal.bind(this)
      this.closeProductsModal = this.closeProductsModal.bind(this)
   }
   handleTextChange(param, event){
      var product = this.state.product;
      product[param] = event.target.value;
      this.setState({product: product})
   }
   handleGridRowsUpdated({ fromRow, toRow, updated }) {
        var sales = this.state.sales
        var key = Object.keys(updated)[0]
        sales.invoiceProductList[fromRow][key] = updated[key]

        sales.invoiceProductList[fromRow].productTotal = this.calculateProductTotal(sales.invoiceProductList[fromRow], true)
        if(key = 'discountPercentage'){
            sales.invoiceProductList[fromRow].actual_discountPercentage = updated[key]
        }

        this.setState({sales: _.cloneDeep(sales)})

        this.calculateSalesTotalPrice()
    }
    handleBatchdRowsUpdated({ fromRow, toRow, updated }) {
         console.log(fromRow, toRow, updated)
         var state = this.state
         var key = Object.keys(updated)[0]
         state.productBatchList[fromRow][key] = updated[key]

         this.setState({productBatchList: state.productBatchList})
         this.addProductToSales()
     }
    calculateProductTotal(product, update) {
         var productTotal = 0;
         var state = this.state;
         if (!product.noDiscount) {

             if (_.isUndefined(product["actual_discountPercentage"] || update) ) {
                product["actual_discountPercentage"] = product.discountPercentage;
             }

             if (state.sales.discountpercent > 0) {
                 console.log(state.sales.discountpercent)
                 product.discountPercentage = state.sales.discountpercent;
             }
             if (state.sales.discountpercent == 0){
                  product.discountPercentage = product["actual_discountPercentage"];
             }


             product.sellingRate = product.mrp - (product.mrp * product.discountPercentage) / 100;

         } else {

             product.sellingRate = product.mrp;
         }
         productTotal = product.sellingRate * product.quantity;

         return productTotal.toFixed(2);
     }
     addProductToSales(){
        var state = this.state;
        var self = this;
        _.forEach(state.productBatchList, function(item, index) {
            if (item.quantity > 0) {
                var new_product = {
                    name: state.selectedProduct.name,
                    purchaseProduct: _.clone(item),
                    quantity: item.quantity
                };
                console.log(item)
                new_product = _.extend(new_product, item)
                console.log(new_product)
                new_product["productTotal"] = new_product.quantity * new_product.sellingRate;
                state.sales.invoiceProductList.push(new_product);
                self.calculateSalesTotalPrice();
            }
        })
        console.log(state)
        this.setState({
          sales: state.sales,
          selectedProduct: '',
          productBatchList: [],
          newSalesProduct: {},
          autoFocus: false,
          individualQty: 0
        })
        this.calculateSalesTotalPrice()
        this.closeProductsModal()
     }
     calculateSalesTotalPrice(){
          var state = this.state;
          state.sales.netAmount = 0;
          var self = this;
         _.forEach(state.sales.invoiceProductList, function(product, index) {
              console.log(product)
              product.productTotal = parseFloat(self.calculateProductTotal(product));
              state.sales.netAmount+= product.productTotal;
          })
          state.sales.netAmount.toFixed(2)
          this.setState({sales: _.cloneDeep(state.sales)})
     }
   saveSales(){
          console.log('hi')
          var self = this
          var state = this.state
          var salesEntry = state.sales
          salesEntry.createdDate = moment(salesEntry.createdDate).format(backEndDateFormat)
          axios.post(salesInfoUrl, salesEntry)
          .then(function (response) {
              console.log(response);
              Materialize.toast("sales Info saved Successfully!!!", 4000, 'green');
              self.reset()
          })
          .catch(function (error) {
            console.log(error);
          });
   }
   getAllProducts(){
     var self = this
     axios.get(productInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setProductList(response.data)
               console.log(response.data)
           }
       });
   }

   getAllSalesMans(){
     var self = this
     axios.get(salesManInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setSalesManList(response.data)
               console.log(response.data)
           }
       });
   }

   setProductList(products){
     this.setState({products: products})
   }

   setSalesManList(vendors){
     this.setState({salesMans:  vendors})
   }
   reset(){
     this.setState({sales: _.clone(samplesales)})
   }
   rowGetter(i) {
      return this.state.sales.invoiceProductList[i];
    }
    batchRowGetter(i) {
       return this.state.productBatchList[i];
     }
    changeCreatedDate(date) {
       var sales = this.state.sales
       sales.createdDate = date
       this.setState({
         sales: sales
       })
   }
   changeSalesMan(salesMan){
       var sales = this.state.sales
       sales.salesMan = salesMan
       this.setState({
         sales: sales
       })
   }
   changeOverallDiscount(event){
      var sales = this.state.sales;
      var value = event.target.value
      sales.discountpercent = value.length ? value : 0;
      this.setState({sales: sales})
      this.calculateSalesTotalPrice()
   }
   changeSelectedProduct(product){
      console.log(product)
      var newSalesProduct = _.clone(product)
       this.setState({
         selectedProduct: product,
         newSalesProduct: newSalesProduct
       })
       this.openProductsModal()
       this.getPurchaseEntryForProduct(product)
   }
   changeQty(event){
      this.setState({individualQty: event.target.value, allotedIndividualQuantity: event.target.value})
   }
   allotQuantity(e) {
        if(e && e.key == 'Enter'){
          var state = this.state
         _.forEach(state.productBatchList, function(item, index) {
             item.quantity = 0;
             if (state.allotedIndividualQuantity <= 0) {
                 return false;
             }
             if (item.stock >= state.allotedIndividualQuantity) {
                 item.quantity = state.allotedIndividualQuantity;
                 state.allotedIndividualQuantity = 0
             } else {
                 item.quantity = item.stock;
                 state.allotedIndividualQuantity = state.allotedIndividualQuantity - item.quantity;
             }
             if (index == state.productBatchList.length - 1 && state.productBatchList > 0) {
                 alert("Not enough stock")
             }
         })

       this.setState({
         productBatchList: state.productBatchList,
         allotedIndividualQuantity: state.allotedIndividualQuantity
       })
       this.addProductToSales()
       this.closeProductsModal()
     }
   }
   getPurchaseEntryForProduct(product){
        var state = this.state;
        var self = this;
        var url = serverLocation + '/productInfo/purchaseProduct/'+product.id
        axios.get(url).
          then(function(response) {
              if (response.status) {
                  if(response.data.length){
                  self.setProductBatchList(response.data)
                  console.log(response.data)
                }else{
                  alert('No stock')
                }
              }
          });
   }
   setProductBatchList(list){
      this.setState({productBatchList: list})
   }
   openProductsModal(){
      this.setState({productsModalOpen: true})
   }
   closeProductsModal(){
      this.setState({productsModalOpen: false, autoFocus: true})
   }
   openStatusMessage(message){
      this.setState({statusMessageOpen: true, statusMessage: message})
   }
   closeStatusMessage(){
      this.setState({statusMessageOpen: false})
   }

   render(){
     var state = this.state;
     var props = this.props;
     var dispalyDateFormate = state.dispalyDateFormate;
     var sales = state.sales;
     var vendors = state.vendors;
     var products = state.products;
     var salesMans = state.salesMans;
     var selectedProduct = state.selectedProduct;
     var newSalesProduct = state.newSalesProduct;
     if(products.length == 0){
        this.getAllProducts()
     }
     if(salesMans.length == 0){
        this.getAllSalesMans()
     }
     const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.closeProductsModal}
        />,
        <FlatButton
          label="Ok"
          primary={true}
          onClick={this.addProductToSales}
        />,
      ];
      return (
        <div>
          <Appbar activeTab = {props.route.activeTab}/>
          <div className="container-fluid top10">

            <div className="row">
                <div>
                    <h4>Sales Entry</h4>
                </div>
                  <div className="col s3">
                    <div>Entry Date</div>
                    <DatePicker
                      dateFormat={dispalyDateFormate}
                      selected={sales.createdDate}
                      onChange={this.changeCreatedDate} />
                  </div>
              <div className="col s3">
                  <div>Sales Man</div>
                  <Select
                    name="Sales Man"
                    labelKey = "name"
                    valueKey = "id"
                    value={sales.salesMan}
                    options={salesMans}
                    onChange={this.changeSalesMan.bind(this)}
                  />
            </div>
            <div className="col s3">
                <TextField
                  style={styles.txtBox}
                  floatingLabelText="Doctor"
                  floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                  value={sales.doctor}
                  onChange={this.handleTextChange.bind(this, 'doctor')} />
            </div>
            <div className="col s3">
                <TextField
                  style={styles.txtBox}
                  floatingLabelText="Customer Name"
                  floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                  value={sales.customerName}
                  onChange={this.handleTextChange.bind(this, 'customerName')} />
            </div>
            </div>
            <div className="row">
              <div className="col s12">
                  <Select
                    autofocus={state.autoFocus}
                    name="Select Product"
                    label= "Select Product"
                    labelKey = "name"
                    valueKey = "id"
                    value={selectedProduct}
                    options={products}
                    onChange={this.changeSelectedProduct.bind(this)}
                  />
              </div>
            </div>
            <div className="row">
            <div className="col s12">
              <ReactDataGrid
                  enableCellSelect={true}
                  columns={salesTableColumns}
                  rowGetter={this.rowGetter}
                  rowsCount={sales.invoiceProductList.length}
                  onGridRowsUpdated={this.handleGridRowsUpdated}
                  minHeight={170}
                  maxHeight={170}
                  />
                <table  fixedHeader = {true} >
                  <tbody displayRowCheckbox={false}>
                      <tr>
                        <td colSpan="9" style={styles.td} ></td>
                        <td colSpan="2" style={styles.td} className=" bold text-right">
                            <TextField
                              style={styles.txtBox}
                              floatingLabelText = "Overall Discount"
                              floatingLabelFocusStyle = {styles.floatingLabelFocusStyle}
                              value={sales.discountpercent}
                              onChange={this.changeOverallDiscount}
                              />
                        </td>
                        <td colSpan="2" style={styles.td} className=" bold text-right">Sales Amount</td>
                        <td style={styles.td} className="text-right"><span>Rs. {sales.netAmount.toFixed(2)}</span></td>
                      </tr>
                  </tbody>
              </table>
            </div>
            </div>
          <div className="row">
             <div className="col offset-s7">
               <button onClick={this.saveSales} type="button" className="waves-effect waves-light blue btn">Save(3)</button>
             </div>
         </div>
      </div>
      <Dialog
        title={newSalesProduct.name}
        actions={actions}
        modal={false}
        open={state.productsModalOpen}
        onRequestClose={this.closeProductsModal}
        autoScrollBodyContent={true}
        >
          <div>
            <div className="col s3 left">
              <TextField
                autoFocus={true}
                style={styles.txtBox}
                floatingLabelText="Qty"
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                onKeyDown={this.allotQuantity}
                onChange={this.changeQty}
                value={state.individualQty} />
            </div>
            <div className="col s3 right">
              <TextField
                style={styles.txtBox}
                floatingLabelText="VAT"
                floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                value={newSalesProduct.vat} />
            </div>
            <ReactDataGrid
                enableCellSelect={true}
                columns={batchListTableColumns}
                rowGetter={this.batchRowGetter}
                rowsCount={state.productBatchList.length}
                onGridRowsUpdated={this.handleBatchdRowsUpdated}
                minHeight={170}
                maxHeight={170}
                />
          </div>
      </Dialog>

      <Snackbar
          open={state.statusMessageOpen}
          message={state.statusMessage}
          autoHideDuration={4000}
          onRequestClose={this.statusMessageClose}
        />
    </div>

      )
  }
}
