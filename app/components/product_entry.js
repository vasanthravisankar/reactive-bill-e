var serverLocation = "http://localhost:8080/api/";
var productInfoUrl = serverLocation + 'productInfo/'

import React from 'react';
import Appbar from './Appbar'
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import Snackbar from 'material-ui/Snackbar';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import moment from 'moment'
import axios from 'axios'
import _ from 'lodash'

var styles = {
  floatingLabelFocusStyle: {
    'fontSize': '18px'
  },
  txtBox: {
    'width': '100%'
  },
  btnStyle: {
    'margin': '12px'
  }
}

var sampleProduct = {
  name: '',
  companyName: '',
  rack: '',
  packedUnit: '',
  purchaseRate: '',
  sellingRate: '',
  mrp: '',
  vat: '',
  preferedSupplier: '',
  productType: '',
  scientificName: '',
  scheduledItem: false,
  narcoticDrug: false,
  scheduledItem: false,
  noDiscount: false
}

/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
 export default class ProductEntry extends React.Component {

   constructor(props) {
     super(props);
     this.state = {
        productsModalOpen: false,
        product: _.clone(sampleProduct),
        products: [],
        update: false,
        statusMessageOpen: false,
        statusMessage: ''
      }

      this.handleTextChange = this.handleTextChange.bind(this)
      this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
      this.reset = this.reset.bind(this)

      this.saveProduct = this.saveProduct.bind(this)
      this.editProduct = this.editProduct.bind(this)
      this.deleteProduct = this.deleteProduct.bind(this)
      this.getAllProducts = this.getAllProducts.bind(this)

      this.openProductsModal = this.openProductsModal.bind(this)
      this.closeProductsModal = this.closeProductsModal.bind(this)
   }
   handleTextChange(param, event){
      var product = this.state.product;
      product[param] = event.target.value;
      this.setState({product: product})
   }
   handleCheckboxChange(param, event, isInputChecked){
       var product = this.state.product;
       product[param] = isInputChecked;
       this.setState({product: product})
   }
   saveProduct(){
     console.log('hi')
        var self = this;
        var state = this.state
        var prod = _.clone(state.product);
        var prodList;
        if (prod == undefined) {
            prodList = state.uploadedJSON;
        } else {
            prodList = [prod];
        }
        if(state.update){
            this.updateProduct(prod)
        }else{
          axios.post(productInfoUrl, prodList)
          .then(function (response) {
            console.log(response);
            self.reset()
          })
          .catch(function (error) {
            console.log(error);
          });
        }
   }
   editProduct(product, event){
        this.setState({product: product, update: true})
        this.closeProductsModal()
   }
   updateProduct(product){
       var self = this;
       var state = this.state;
       axios.put(productInfoUrl, product).
        then(function(response) {
            if (response.status) {
                self.reset()
                Materialize.toast("Product updated Successfully!!!", 4000, 'green');
                console.log(response.data)
            }
        });
   }
   deleteProduct(product, event){
     var self = this;
     var state = this.state
       axios.delete(productInfoUrl+product.id).
          then(function(response) {
              if (response.status) {
                  self.closeProductsModal()
                  Materialize.toast("Product deleted Successfully!!!", 4000, 'green');
                  console.log(response.data)
              }
          });
   }
   getAllProducts(){
     var self = this
     axios.get(productInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setProductList(response.data)
               console.log(response.data)
           }
       });
   }
   upload(){
     console.log('hi')
   }
   setProductList(products){
     this.setState({products: products})
     this.openProductsModal()
   }
   openProductsModal(){
     this.setState({productsModalOpen: true})
   }
   closeProductsModal(){
     this.setState({productsModalOpen: false})
   }
   openStatusMessage(message){
      this.setState({statusMessageOpen: true, statusMessage: message})
   }
   closeStatusMessage(){
      this.setState({statusMessageOpen: false})
   }
   reset(){
     console.log(sampleProduct)
     this.setState({product: _.clone(sampleProduct)})
   }
   getProducsTableRows(){
      var producRows = []
      var self = this
      producRows = _.map(this.state.products, function(product, index){
            return <tr key={index}>
                      <td>{product.name}</td>
                      <td>{product.rack}</td>
                      <td>{product.mrp}</td>
                      <td>
                          <i onClick={self.editProduct.bind(this, product)} style={{'cursor': 'pointer'}} className="material-icons blue-text">edit</i>
                          <i onClick={self.deleteProduct.bind(this, product)} style={{'cursor': 'pointer'}} className="material-icons red-text">delete</i>
                      </td>
                  </tr>
      })

      return producRows
   }

   render(){
     var state = this.state;
     var props = this.props;
     var dispalyDateFormate = state.dispalyDateFormate;
     var product = state.product;
     const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.closeProductsModal}
        />,
        <FlatButton
          label="Ok"
          primary={true}
          onClick={this.closeProductsModal}
        />,
      ];
      return (
        <div>
          <Appbar activeTab = {props.route.activeTab}/>
          <div className="container-fluid" style={{'padding':'20px'}}>
               <div className="row">
                  <div>
                      <h4>Product Entry</h4>
                  </div>
                  <div className="col s12">
                     <button onClick={this.getAllProducts.bind(this)} className="btn right">View All Products</button>
                  </div>
               </div>

              <form name="productForm" id="productForm">
                   <div className="row">
                      <div className="col s3">
                         <TextField
                           style = {styles.txtBox}
                           autoFocus={true}
                           floatingLabelText="Product Name"
                           floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                          value={product.name}
                          onChange={this.handleTextChange.bind(this, 'name')}
                          />
                      </div>
                      <div className="col s3">
                          <TextField
                            style = {styles.txtBox}
                            floatingLabelText="Company Name"
                            floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                           value={product.companyName}
                           onChange={this.handleTextChange.bind(this, 'companyName')}
                           />
                        </div>
                         <div className="col s3">
                           <TextField
                             style = {styles.txtBox}
                             floatingLabelText="Rack"
                             floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                            value={product.rack}
                            onChange={this.handleTextChange.bind(this, 'rack')}
                            />
                          </div>
                          <div className="col s3">
                            <TextField
                              style = {styles.txtBox}
                              floatingLabelText="Packed Unit"
                              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                             value={product.packedUnit}
                             onChange={this.handleTextChange.bind(this, 'packedUnit')}
                             />
                           </div>
                   </div>
                   <div className="row">
                     <div className="col s3">
                         <TextField
                           style = {styles.txtBox}
                           floatingLabelText="P. Rate"
                           floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                          value={product.purchaseRate}
                          onChange={this.handleTextChange.bind(this, 'purchaseRate')}
                          />
                       </div>
                        <div className="col s3">
                          <TextField
                            style = {styles.txtBox}
                            floatingLabelText="S. Rate"
                            floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                           value={product.sellingRate}
                           onChange={this.handleTextChange.bind(this, 'sellingRate')}
                           />
                         </div>
                         <div className="col s3">
                           <TextField
                             style = {styles.txtBox}
                             floatingLabelText="MRP"
                             floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                            value={product.mrp}
                            onChange={this.handleTextChange.bind(this, 'mrp')}
                            />
                          </div>
                          <div className="col s3">
                             <TextField
                               style = {styles.txtBox}
                               floatingLabelText="VAT %"
                               floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                              value={product.vat}
                              onChange={this.handleTextChange.bind(this, 'vat')}
                              />
                          </div>
                   </div>
                   <div className="row">
                        <div className="col s2">
                            <Checkbox
                              checked={product.scheduledItem}
                              label="Scheduled Item"
                              onCheck={this.handleCheckboxChange.bind(this, 'scheduledItem')}
                            />
                        </div>
                        <div className="col s2">
                            <Checkbox
                              checked={product.surgicalItem}
                              label="Surgical Item"
                              onCheck={this.handleCheckboxChange.bind(this, 'surgicalItem')}
                            />
                        </div>
                        <div className="col s2">
                              <Checkbox
                                checked={product.narcoticDrug}
                                label="Narcotic Drug"
                                onCheck={this.handleCheckboxChange.bind(this, 'narcoticDrug')}
                              />
                        </div>
                        <div className="col s2">
                              <Checkbox
                                checked={product.noDiscount}
                                label="No Discount"
                                onCheck={this.handleCheckboxChange.bind(this, 'noDiscount')}
                              />
                        </div>
                   </div>
                   <div className="row">
                        <div className="col s4">
                            <TextField
                              style = {styles.txtBox}
                              floatingLabelText="Scientific Name"
                              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                             value={product.scientificName}
                             onChange={this.handleTextChange.bind(this, 'scientificName')}
                             />
                        </div>
                        <div className="col s4">
                            <TextField
                              style = {styles.txtBox}
                              floatingLabelText="Product Type"
                              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                             value={product.productType}
                             onChange={this.handleTextChange.bind(this, 'productType')}
                             />
                        </div>
                        <div className="col s4">
                            <TextField
                              style = {styles.txtBox}
                              floatingLabelText="Prefered Supplier"
                              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                             value={product.preferedSupplier}
                             onChange={this.handleTextChange.bind(this, 'preferedSupplier')}
                             />
                        </div>
                   </div>
                   <div className="row">
                        <div className="col offset-s5" >
                           <div className="file-field col s5" >
                              <div className="btn grey">
                                 <span>Upload XLS&hellip;</span>
                                 <input type="file" id="my_file_input" multiple />
                              </div>
                              <div className="file-path-wrapper">
                                 <input className="file-path validate" type="text" readOnly />
                              </div>
                           </div>
                           <RaisedButton onClick={this.upload.bind(this)} label="Upload" secondary={true} style={styles.btnStyle} />
                           <RaisedButton onClick={this.reset.bind(this)} label="Reset" secondary={true} style={styles.btnStyle} />
                           <RaisedButton onClick={this.saveProduct.bind(this)} label="Save" primary={true} style={styles.btnStyle} />
                        </div>
                   </div>

                   </form>
              </div>
              <Dialog
                title="Products"
                actions={actions}
                modal={false}
                open={state.productsModalOpen}
                onRequestClose={this.closeProductsModal}
                autoScrollBodyContent={true}
                >
                  <div>
                    <table
                        className="bordered responsive-table highlight"
                        style={{"height": "300px"}}
                      >
                        <thead >
                          <tr>
                            <th>Name</th>
                            <th>Rack</th>
                            <th>S. Rate</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.getProducsTableRows()}
                        </tbody>
                    </table>
                  </div>
              </Dialog>
              <Snackbar
                  open={state.statusMessageOpen}
                  message={state.statusMessage}
                  autoHideDuration={4000}
                  onRequestClose={this.statusMessageClose}
                />
            </div>

      )
  }
}
