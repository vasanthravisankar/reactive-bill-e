var serverLocation = "http://localhost:8080/api/";
var vendorInfoUrl = serverLocation + 'vendorInfo/'

import React from 'react';
import Appbar from './Appbar'
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';

import moment from 'moment'
import axios from 'axios'
import _ from 'lodash'

var styles = {
  floatingLabelFocusStyle: {
    'fontSize': '18px'
  },
  btnStyle: {
    'margin': '12px'
  },
  txtBox: {
      'width': '100%'
  }
}

var sampleVendor = {
  name: '',
  address: '',
  phoneNo: ''
}

/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
 export default class vendorEntry extends React.Component {

   constructor(props) {
     super(props);
     this.state = {
        vendorsModalOpen: false,
        vendor: _.clone(sampleVendor),
        vendors: [],
        update: false,
        statusMessageOpen: false,
        statusMessage: ''
      }

      this.handleTextChange = this.handleTextChange.bind(this)

      this.reset = this.reset.bind(this)

      this.saveVendor = this.saveVendor.bind(this)
      this.editVendor = this.editVendor.bind(this)
      this.deleteVendor = this.deleteVendor.bind(this)
      this.getAllVendors = this.getAllVendors.bind(this)

      this.openVendorsModal = this.openVendorsModal.bind(this)
      this.closeVendorsModal = this.closeVendorsModal.bind(this)
   }
   handleTextChange(param, event){
      var vendor = this.state.vendor;
      vendor[param] = event.target.value;
      this.setState({vendor: vendor})
   }
   saveVendor(){
     console.log('hi')
        var self = this;
        var state = this.state
        var vendor = _.clone(state.vendor);
        var vendorList;
        if (vendor == undefined) {
            vendorList = state.uploadedJSON;
        } else {
            vendorList = [vendor];
        }
        if(state.update){
            this.updateVendor(vendor)
        }else{
          axios.post(vendorInfoUrl, vendorList)
          .then(function (response) {
            console.log(response);
            self.reset()
          })
          .catch(function (error) {
            console.log(error);
          });
        }
   }
   editVendor(vendor, event){
        this.setState({vendor: vendor, update: true})
        this.closeVendorsModal()
   }
   updateVendor(vendor){
       var self = this;
       var state = this.state;
       axios.put(vendorInfoUrl, vendor).
        then(function(response) {
            if (response.status) {
                self.reset()
                Materialize.toast("vendor updated Successfully!!!", 4000, 'green');
                console.log(response.data)
            }
        });
   }
   deleteVendor(vendor, event){
     var self = this;
     var state = this.state
       axios.delete(vendorInfoUrl+vendor.id).
          then(function(response) {
              if (response.status) {
                  self.closevendorsModal()
                  Materialize.toast("vendor deleted Successfully!!!", 4000, 'green');
                  console.log(response.data)
              }
          });
   }
   getAllVendors(){
     var self = this
     axios.get(vendorInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setVendorList(response.data)
               console.log(response.data)
           }
       });
   }
   upload(){
     console.log('hi')
   }
   setVendorList(vendors){
     this.setState({vendors: vendors})
     this.openVendorsModal()
   }
   openVendorsModal(){
     this.setState({vendorsModalOpen: true})
   }
   closeVendorsModal(){
     this.setState({vendorsModalOpen: false})
   }
   openStatusMessage(message){
      this.setState({statusMessageOpen: true, statusMessage: message})
   }
   closeStatusMessage(){
      this.setState({statusMessageOpen: false})
   }
   reset(){
     this.setState({vendor: _.clone(sampleVendor)})
   }
   getVendorsTableRows(){
      var producRows = []
      var self = this
      producRows = _.map(this.state.vendors, function(vendor, index){
            return <tr key={index}>
                      <td>{vendor.name}</td>
                      <td>{vendor.phoneNo}</td>
                      <td>{vendor.address}</td>
                      <td>{vendor.salary}</td>
                      <td>
                          <i onClick={self.editVendor.bind(this, vendor)} style={{'cursor': 'pointer'}} className="material-icons blue-text">edit</i>
                          <i onClick={self.deleteVendor.bind(this, vendor)} style={{'cursor': 'pointer'}} className="material-icons red-text">delete</i>
                      </td>
                  </tr>
      })

      return producRows
   }

   render(){
     var state = this.state;
     var props = this.props;
     var vendor = state.vendor;
     const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.closeVendorsModal}
        />,
        <FlatButton
          label="Ok"
          primary={true}
          onClick={this.closeVendorsModal}
        />,
      ];
      return (
        <div>
          <Appbar activeTab = {props.route.activeTab}/>
          <div className="container-fluid" style={{'padding':'20px'}}>
               <div className="row">
                 <div>
                     <h4>Vendor Entry</h4>
                 </div>
                  <div className="col s12">
                     <button onClick={this.getAllVendors.bind(this)} className="btn right">View All vendors</button>
                  </div>
               </div>

              <form name="vendorForm" id="vendorForm">
                   <div className="row">
                      <div className="col s4">
                         <TextField
                           autoFocus={true}
                           style = {styles.txtBox}
                           floatingLabelText="Supplier Name"
                           floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                          value={vendor.name}
                          onChange={this.handleTextChange.bind(this, 'name')}
                          />
                      </div>
                      <div className="col s4">
                          <TextField
                            style = {styles.txtBox}
                            floatingLabelText="Address"
                            floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                           value={vendor.address}
                           onChange={this.handleTextChange.bind(this, 'address')}
                           />
                        </div>
                         <div className="col s4">
                           <TextField
                            style = {styles.txtBox}
                             floatingLabelText="Phone No"
                             floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                            value={vendor.phoneNo}
                            onChange={this.handleTextChange.bind(this, 'phoneNo')}
                            />
                          </div>
                   </div>
                   <div className="row">
                        <div className="col offset-s5" >
                           <RaisedButton onClick={this.reset.bind(this)} label="Reset" secondary={true} style={styles.btnStyle} />
                           <RaisedButton onClick={this.saveVendor.bind(this)} label="Save" primary={true} style={styles.btnStyle} />
                        </div>
                   </div>

                   </form>
              </div>
              <Dialog
                title="Sales Mans"
                actions={actions}
                modal={false}
                open={state.vendorsModalOpen}
                onRequestClose={this.closeVendorsModal}
                autoScrollBodyContent={true}
                >
                  <div>
                    <table
                        className="bordered responsive-table highlight"
                        style={{"height": "300px"}}
                      >
                        <thead >
                          <tr>
                            <th>Name</th>
                            <th>Mobile No</th>
                            <th>Address</th>
                            <th>Salary</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.getVendorsTableRows()}
                        </tbody>
                    </table>
                  </div>
              </Dialog>
              <Snackbar
                  open={state.statusMessageOpen}
                  message={state.statusMessage}
                  autoHideDuration={4000}
                  onRequestClose={this.statusMessageClose}
                />
            </div>

      )
  }
}
