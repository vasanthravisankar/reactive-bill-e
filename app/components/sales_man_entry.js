var serverLocation = "http://localhost:8080/api/";
var salesManInfoUrl = serverLocation + 'salesManInfo/'

import React from 'react';
import Appbar from './Appbar'
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import Snackbar from 'material-ui/Snackbar';
import TextField from 'material-ui/TextField';
import moment from 'moment'
import axios from 'axios'
import _ from 'lodash'

var styles = {
  floatingLabelFocusStyle: {
    'fontSize': '18px'
  },
  btnStyle: {
    'margin': '12px'
  },
  txtBox: {
      'width': '100%'
  }
}

var samplesalesMan = {
  name: '',
  address: '',
  salary: '',
  mobileNo: ''
}

/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
 export default class salesManEntry extends React.Component {

   constructor(props) {
     super(props);
     this.state = {
        salesMansModalOpen: false,
        salesMan: _.clone(samplesalesMan),
        salesMans: [],
        update: false,
        statusMessageOpen: false,
        statusMessage: ''
      }

      this.handleTextChange = this.handleTextChange.bind(this)

      this.reset = this.reset.bind(this)

      this.saveSalesMan = this.saveSalesMan.bind(this)
      this.editSalesMan = this.editSalesMan.bind(this)
      this.deleteSalesMan = this.deleteSalesMan.bind(this)
      this.getAllSalesMans = this.getAllSalesMans.bind(this)

      this.openSalesMansModal = this.openSalesMansModal.bind(this)
      this.closeSalesMansModal = this.closeSalesMansModal.bind(this)
   }
   handleTextChange(param, event){
      var salesMan = this.state.salesMan;
      salesMan[param] = event.target.value;
      this.setState({salesMan: salesMan})
   }
   saveSalesMan(){
     console.log('hi')
        var self = this;
        var state = this.state
        var salesMan = _.clone(state.salesMan);
        var salesManList;
        if (salesMan == undefined) {
            salesManList = state.uploadedJSON;
        } else {
            salesManList = [salesMan];
        }
        if(state.update){
            this.updateSalesMan(salesMan)
        }else{
          axios.post(salesManInfoUrl, salesManList)
          .then(function (response) {
            console.log(response);
            self.reset()
          })
          .catch(function (error) {
            console.log(error);
          });
        }
   }
   editSalesMan(salesMan, event){
        this.setState({salesMan: salesMan, update: true})
        this.closeSalesMansModal()
   }
   updateSalesMan(salesMan){
       var self = this;
       var state = this.state;
       axios.put(salesManInfoUrl, salesMan).
        then(function(response) {
            if (response.status) {
                self.reset()
                console.log(response.data)
            }
        });
   }
   deleteSalesMan(salesMan, event){
     var self = this;
     var state = this.state
       axios.delete(salesManInfoUrl+salesMan.id).
          then(function(response) {
              if (response.status) {
                  self.closesalesMansModal()
                  self.reset()
                  console.log(response.data)
              }
          });
   }
   getAllSalesMans(){
     var self = this
     axios.get(salesManInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setSalesManList(response.data)
               console.log(response.data)
           }
       });
   }
   upload(){
     console.log('hi')
   }
   setSalesManList(salesMans){
     this.setState({salesMans: salesMans})
     this.openSalesMansModal()
   }
   openSalesMansModal(){
     this.setState({salesMansModalOpen: true})
   }
   closeSalesMansModal(){
     this.setState({salesMansModalOpen: false})
   }
   openStatusMessage(message){
      this.setState({statusMessageOpen: true, statusMessage: message})
   }
   closeStatusMessage(){
      this.setState({statusMessageOpen: false})
   }
   reset(){
     this.setState({salesMan: _.clone(samplesalesMan)})
   }
   getSalesMansTableRows(){
      var producRows = []
      var self = this
      producRows = _.map(this.state.salesMans, function(salesMan, index){
            return <tr key={index}>
                      <td>{salesMan.name}</td>
                      <td>{salesMan.mobileNo}</td>
                      <td>{salesMan.address}</td>
                      <td>{salesMan.salary}</td>
                      <td>
                          <i onClick={self.editSalesMan.bind(this, salesMan)} style={{'cursor': 'pointer'}} className="material-icons blue-text">edit</i>
                          <i onClick={self.deleteSalesMan.bind(this, salesMan)} style={{'cursor': 'pointer'}} className="material-icons red-text">delete</i>
                      </td>
                  </tr>
      })

      return producRows
   }

   render(){
     var state = this.state;
     var props = this.props;
     var salesMan = state.salesMan;
     const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.closeSalesMansModal}
        />,
        <FlatButton
          label="Ok"
          primary={true}
          onClick={this.closeSalesMansModal}
        />,
      ];
      return (
        <div>
          <Appbar activeTab = {props.route.activeTab}/>
          <div className="container-fluid" style={{'padding':'20px'}}>
               <div className="row">
                 <div>
                     <h4>Sales Man Entry</h4>
                 </div>
                  <div className="col s12">
                     <button onClick={this.getAllSalesMans.bind(this)} className="btn right">View All salesMans</button>
                  </div>
               </div>

              <form name="salesManForm" id="salesManForm">
                   <div className="row">
                      <div className="col s3">
                         <TextField
                           autoFocus={true}
                           style = {styles.txtBox}
                           floatingLabelText="Sales Man Name"
                           floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                          value={salesMan.name}
                          onChange={this.handleTextChange.bind(this, 'name')}
                          />
                      </div>
                      <div className="col s3">
                          <TextField
                            style = {styles.txtBox}
                            floatingLabelText="Address"
                            floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                           value={salesMan.address}
                           onChange={this.handleTextChange.bind(this, 'address')}
                           />
                        </div>
                         <div className="col s3">
                           <TextField
                            style = {styles.txtBox}
                             floatingLabelText="Mobile No"
                             floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                            value={salesMan.mobileNo}
                            onChange={this.handleTextChange.bind(this, 'mobileNo')}
                            />
                          </div>
                          <div className="col s3">
                            <TextField
                              style = {styles.txtBox}
                              floatingLabelText="Salary"
                              floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                             value={salesMan.salary}
                             onChange={this.handleTextChange.bind(this, 'salary')}
                             />
                           </div>
                   </div>
                   <div className="row">
                        <div className="col offset-s5" >
                           <RaisedButton onClick={this.reset.bind(this)} label="Reset" secondary={true} style={styles.btnStyle} />
                           <RaisedButton onClick={this.saveSalesMan.bind(this)} label="Save" primary={true} style={styles.btnStyle} />
                        </div>
                   </div>

                   </form>
              </div>
              <Dialog
                title="Sales Mans"
                actions={actions}
                modal={false}
                open={state.salesMansModalOpen}
                onRequestClose={this.closeSalesMansModal}
                autoScrollBodyContent={true}
                >
                  <div>
                    <table
                        className="bordered responsive-table highlight"
                        style={{"height": "300px"}}
                      >
                        <thead >
                          <tr>
                            <th>Name</th>
                            <th>Mobile No</th>
                            <th>Address</th>
                            <th>Salary</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.getSalesMansTableRows()}
                        </tbody>
                    </table>
                  </div>
              </Dialog>

              <Snackbar
                  open={state.statusMessageOpen}
                  message={state.statusMessage}
                  autoHideDuration={4000}
                  onRequestClose={this.statusMessageClose}
                />
            </div>

      )
  }
}
