var serverLocation = "http://localhost:8080/api/";
var productInfoUrl = serverLocation + 'productInfo/'
var vendorInfoUrl = serverLocation + 'vendorInfo/'
var salesManInfoUrl = serverLocation + 'salesManInfo/'
var purchaseInfoUrl = serverLocation + '/purchaseInfo/'

var backEndDateFormat = 'YYYY-MM-DD'

import React from 'react';
import Appbar from './Appbar'
import Dialog from 'material-ui/Dialog';
import DatePicker from 'react-datepicker'
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import Snackbar from 'material-ui/Snackbar';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import ReactDataGrid from 'react-data-grid';
import moment from 'moment'
import axios from 'axios'
import _ from 'lodash'

import Select from 'react-select';

var styles = {
  floatingLabelFocusStyle: {
    'fontSize': '18px'
  },
  txtBox: {
      'width': '100%'
  },
  td: {
      'padding': '5px'
  },
  btnStyle: {
    'margin': '12px'
  }
}

var purchaseTableColumns =   [
            {
              key: 'name',
              name: 'Name',
              width: 130
            },
            {
              key: 'packageUnit',
              name: 'pkd unit',
              width: 70
            },
            {
              key: 'batchNo',
              name: 'Batch',
              editable: true,
              width: 100
            },
            {
              key: 'expiryDate',
              name: 'Exp. Date',
              editable: true,
              width: 70
            },
            {
              key: 'stock',
              name: 'Qty',
              editable: true,
              width: 60
            },
            {
              key: 'freeUnit',
              name: 'Free',
              editable: true,
              width: 60
            },
            {
              key: 'purchaseRate',
              name: 'P. Rate',
              width: 70
            },
            {
              key: 'mrp',
              name: 'MRP',
              width: 70
            },
            {
              key: 'discountPercentage',
              name: 'Disc %',
              editable: true,
              width: 60
            },
            {
              key: 'sellingRate',
              name: 'S. Rate',
              width: 70
            },
            {
              key: 'vat',
              name: 'vat',
              width: 40
            },
            {
              key: 'productTotal',
              name: 'Total',
              width: 100
            },
            {
              key: 'actions',
              name: 'Actions',
              width: 60
            },

    ]

var samplePurchase = {
    entryNo: '',
    entryDate: moment(),
    billDate: moment(),
    vendor: {},
    salesMan: {},
    billNo: '',
    paymentAmount: '',
    purchaseProducts: [],
    vat: {
          five: 0,
          fourteen: 0,
          exempted: 0,
          total: 0
      },
    purchaseTotal: 0,
    paymentAmount: 0
    }
/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
 export default class PurchaseEntry extends React.Component {

   constructor(props) {
     super(props);
     this.state = {
        productsModalOpen: false,
        backEndDateFormat: 'YYYY-MM-DD',
        dispalyDateFormate: "DD-MM-YYYY",
        selectedProduct: '',
        newPurchaseProduct: {},
        purchase: _.clone(samplePurchase),
        vendors: [],
        products: [],
        salesMans: [],
        update: false,
        autoFocus: false,
        statusMessageOpen: false,
        statusMessage: ''
      }

      this.handleTextChange = this.handleTextChange.bind(this)
      this.handleNewProductChange = this.handleNewProductChange.bind(this)
      this.checkEnterKeyPress = this.checkEnterKeyPress.bind(this)
      this.changeEntryDate = this.changeEntryDate.bind(this)
      this.changeBillDate = this.changeBillDate.bind(this)
      this.changeVendor = this.changeVendor.bind(this)
      this.changeSalesMan = this.changeSalesMan.bind(this)
      this.addProductToPurchase = this.addProductToPurchase.bind(this)
      this.calculateVAT = this.calculateVAT.bind(this)
      this.calculatePurchaseTotalPrice = this.calculatePurchaseTotalPrice.bind(this)
      this.rowGetter = this.rowGetter.bind(this)
      this.handleGridRowsUpdated = this.handleGridRowsUpdated.bind(this)
      this.reset = this.reset.bind(this)

      this.savePurchase = this.savePurchase.bind(this)

      this.getAllProducts = this.getAllProducts.bind(this)

      this.openProductsModal = this.openProductsModal.bind(this)
      this.closeProductsModal = this.closeProductsModal.bind(this)
   }
   handleTextChange(param, event){
      var product = this.state.product;
      product[param] = event.target.value;
      this.setState({product: product})
   }
   handleNewProductChange(param, event){
      if(event.keyCode == 13){
          this.addProductToPurchase()
      }else{
        var product = this.state.newPurchaseProduct;
        product[param] = event.target.value;
        if(param == 'mrp' || param == 'discountPercentage'){
            product.sellingRate = this.calculateSellingRate(product)
        }
        product.productTotal = this.calculateProductTotal(product)
        this.setState({newPurchaseProduct: product})
      }
   }
   checkEnterKeyPress(e){
      if(e.key=='Enter'){
        this.addProductToPurchase()
      }
   }
   handleGridRowsUpdated({ fromRow, toRow, updated }) {
        console.log(fromRow, toRow, updated)
        var purchase = this.state.purchase
        var key = Object.keys(updated)[0]
        purchase.purchaseProducts[fromRow][key] = updated[key]
        purchase.purchaseProducts[fromRow].productTotal = this.calculateProductTotal(purchase.purchaseProducts[fromRow])

        if(key == 'mrp' || key == 'discountPercentage'){
            purchase.purchaseProducts[fromRow].sellingRate = this.calculateSellingRate(purchase.purchaseProducts[fromRow])
        }
        this.calculatePurchaseTotalPrice()
        this.calculateVAT()
        this.setState({purchase: purchase})
    }

   calculateSellingRate(product) {
        var sellingRate = 0;
        if (!_.isUndefined(product.mrp) && !_.isUndefined(product.discountPercentage)) {
            sellingRate = product.mrp - (product.mrp * product.discountPercentage) / 100
        }
        return sellingRate.toFixed(2);
    }
    calculateProductTotal(product) {
         var productTotal = 0;
         productTotal = product.purchaseRate*(product.stock - product.freeUnit)

         return productTotal.toFixed(2);
     }
     addProductToPurchase(){
        var state = this.state
        var newPurchaseProduct = _.clone(state.newPurchaseProduct)
        newPurchaseProduct.actions = this.getActions(state.purchase.purchaseProducts.length);
        state.purchase.purchaseProducts.push(newPurchaseProduct)
        this.setState({
          purchase: state.purchase,
          selectedProduct: '',
          newPurchaseProduct: {},
          autoFocus: false
        })
        this.calculatePurchaseTotalPrice()
        this.calculateVAT()
        this.closeProductsModal()
     }
     calculateVAT(){
        var state = this.state
        var vat = {
              five: 0,
              fourteen: 0,
              exempted: 0,
              total: 0
          }
         _.forEach(state.purchase.purchaseProducts, function(product) {
               switch (product.vat) {
                   case 5:
                      if(!_.isUndefined(product.productTotal)){
                            vat.five += product.productTotal * 5 / 100;
                        }
                       break;
                   case 14:
                       if(!_.isUndefined(product.productTotal)){
                             vat.fourteen += product.productTotal * 14 / 100;
                     }
                       break;
               }
           })

           vat.total = vat.five + vat.fourteen;
           state.purchase.vat = vat;
           state.purchase.paymentAmount = state.purchase.purchaseTotal +  state.purchase.vat.total
           this.setState({purchase: state.purchase})
     }
     calculatePurchaseTotalPrice(){
          var state = this.state;
          state.purchase.purchaseTotal = 0;
         _.forEach(state.purchase.purchaseProducts, function(product) {
              if (!_.isUndefined(product.productTotal)) {
                  state.purchase.purchaseTotal += parseInt(product.productTotal);
              }
          })
          this.setState({purchase: state.purchase})
     }
     getActions(index){
        return (
          <div>
            <i onClick={this.deleteRow.bind(this, index)} className="material-icons">delete</i>
          </div>
        )
     }
     deleteRow(index, event){
          var self = this;
          var purchase = this.state.purchase;
          purchase.purchaseProducts.splice(index, 1);
          _.forEach(purchase.purchaseProducts, function(product, i){
              product.actions = self.getActions(i)
          })
          this.setState({purchase: purchase})
     }
     savePurchase(){
          console.log('hi')
          var self = this
          var state = this.state
          var purchaseEntry = [state.purchase]
          axios.post(purchaseInfoUrl, purchaseEntry)
          .then(function (response) {
              console.log(response);
              self.openStatusMessage("Purchase saved successfully")
              self.reset()
          })
          .catch(function (error) {
            console.log(error);
          });
   }
   getAllProducts(){
     var self = this
     axios.get(productInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setProductList(response.data)
               console.log(response.data)
           }
       });
   }
   getAllVendors(){
     var self = this
     axios.get(vendorInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setVendorList(response.data)
               console.log(response.data)
           }
       });
   }
   getAllSalesMans(){
     var self = this
     axios.get(salesManInfoUrl).
       then(function(response) {
           if (response.status) {
               self.setSalesManList(response.data)
               console.log(response.data)
           }
       });
   }

   setProductList(products){
     this.setState({products: products})
   }
   setVendorList(vendors){
     this.setState({vendors:  vendors})
   }
   setSalesManList(vendors){
     this.setState({salesMans:  vendors})
   }
   reset(){
     this.setState({purchase: _.clone(samplePurchase)})
   }
   rowGetter(i) {
      return this.state.purchase.purchaseProducts[i];
    }
   changeEntryDate(date) {
       var purchase = this.state.purchase
       purchase.entryDate = date
       this.setState({
         purchase: purchase
       })
   }
   changeBillDate(date) {
       var purchase = this.state.purchase
       purchase.billDate = date
       this.setState({
         purchase: purchase
       })
   }
   changeVendor(vendor){
       var purchase = this.state.purchase
       purchase.vendor = vendor
       this.setState({
         purchase: purchase
       })
   }

   changeSalesMan(salesMan){
       var purchase = this.state.purchase
       purchase.salesMan = salesMan
       this.setState({
         purchase: purchase
       })
   }
   changeSelectedProduct(product){
      console.log(product)
      var newPurchaseProduct = _.clone(product)
      newPurchaseProduct.stock = 0;
      newPurchaseProduct.freeUnit = 0;
      newPurchaseProduct.discountPercentage = 8;
       this.setState({
         selectedProduct: product,
         newPurchaseProduct: newPurchaseProduct
       })
       this.openProductsModal()
   }
   openProductsModal(){
      this.setState({productsModalOpen: true})
   }
   closeProductsModal(){
      this.setState({productsModalOpen: false, autoFocus: true})
   }
   openStatusMessage(message){
      this.setState({statusMessageOpen: true, statusMessage: message})
   }
   closeStatusMessage(){
      this.setState({statusMessageOpen: false})
   }

   render(){
     var state = this.state;
     var props = this.props;
     var dispalyDateFormate = state.dispalyDateFormate;
     var purchase = state.purchase;
     var vendors = state.vendors;
     var products = state.products;
     var salesMans = state.salesMans;
     var selectedProduct = state.selectedProduct;
     var newPurchaseProduct = state.newPurchaseProduct;
     if(products.length == 0){
        this.getAllProducts()
     }
     if(vendors.length == 0){
        this.getAllVendors()
     }
     if(salesMans.length == 0){
        this.getAllSalesMans()
     }
     const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.closeProductsModal}
        />,
        <FlatButton
          label="Ok"
          primary={true}
          onClick={this.addProductToPurchase}
        />,
      ];
      return (
        <div>
          <Appbar activeTab = {props.route.activeTab}/>
          <div className="container-fluid top10">
            <div className="row">
                  <div>
                      <h4>Purchase Entry</h4>
                  </div>
                  <div className="col s4">
                    <TextField
                      style={styles.txtBox}
                      floatingLabelText="Entry No"
                      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                      value={purchase.entryNo}
                     onChange={this.handleTextChange.bind(this, 'entryNo')}
                     />
                  </div>
                  <div className="col s4">
                    <div>Entry Date</div>
                    <DatePicker
                      dateFormat={dispalyDateFormate}
                      selected={purchase.entryDate}
                      onChange={this.changeEntryDate} />
                  </div>
                  <div className="col s4">
                    <div>Bill Date</div>
                    <DatePicker
                      dateFormat={dispalyDateFormate}
                      selected={purchase.billDate}
                      onChange={this.changeBillDate} />
                  </div>
            </div>
            <div className="row">
                <div className="col s3">
                    <div>Supplier</div>
                    <Select
                    	name="Supplier"
                      labelKey = "name"
                      valueKey = "id"
                      value={purchase.vendor}
                      options={vendors}
                    	onChange={this.changeVendor.bind(this)}
                    />
                </div>
            <div className="col s3">
                  <div>Sales Man</div>
                  <Select
                    name="Sales Man"
                    label="Sales Man"
                    labelKey = "name"
                    valueKey = "id"
                    value={purchase.salesMan}
                    options={salesMans}
                    onChange={this.changeSalesMan.bind(this)}
                  />
            </div>
            <div className="col s3">
                <TextField
                  style={styles.txtBox}
                  floatingLabelText="Bill No"
                  floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                  value={purchase.billNo}
                  onChange={this.handleTextChange.bind(this, 'billNo')} />
            </div>
            <div className="col s3">
                <TextField
                  style={styles.txtBox}
                  floatingLabelText="Bill Amount"
                  floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                  value={purchase.paymentAmount}
                  onChange={this.handleTextChange.bind(this, 'paymentAmount')} />
            </div>
            </div>
            <div className="row">
              <div className="col s12">
                  <Select
                    autofocus={state.autoFocus}
                    name="Select Product"
                    label= "Select Product"
                    labelKey = "name"
                    valueKey = "id"
                    value={selectedProduct}
                    options={products}
                    onChange={this.changeSelectedProduct.bind(this)}
                  />
              </div>
            </div>
            <div className="row">
              { purchase.purchaseProducts.length ?
                <div className="col s12">
                  <ReactDataGrid
                      enableCellSelect={true}
                      columns={purchaseTableColumns}
                      rowGetter={this.rowGetter}
                      rowsCount={purchase.purchaseProducts.length}
                      onGridRowsUpdated={this.handleGridRowsUpdated}
                      minHeight={170}
                      maxHeight={170}
                      />
                    <div style={{"marginTop": "7px"}}>
                        <span className="text-right" style={{'marginLeft':'680px', 'marginRight':'10px'}}>
                            Purchase Amount
                        </span>
                        <span className="bold text-right" >
                            Rs. {purchase.purchaseTotal}
                        </span>
                    </div>
                    <div style={{"marginTop": "7px"}}>
                        <span className=" bold text-right" style={{'marginLeft':'300px', 'marginRight':'10px'}}>
                            VAT
                        </span>
                        <span className="text-right" style={{'marginRight':'10px'}}>
                             5% : Rs. {purchase.vat.five}
                        </span>
                        <span className="text-right" style={{'marginRight':'10px'}}>
                             14% : Rs. {purchase.vat.fourteen}
                        </span>
                        <span className="text-right" style={{'marginRight':'165px'}}>
                             Exempted : Rs. {purchase.vat.exempted}
                        </span>
                        <span className="bold text-right" >
                             Total : Rs. {purchase.vat.total}
                        </span>
                    </div>
                    <div style={{"marginTop": "7px"}}>
                        <span className=" bold text-right" style={{'marginLeft':'760px', 'marginRight':'10px'}}>
                            Total
                        </span>
                        <span className=" bold text-right">
                          Rs: {purchase.paymentAmount}
                        </span>
                    </div>
                    {/*<table  fixedHeader = {true} >
                      <tbody displayRowCheckbox={false}>
                          <tr>
                            <td colSpan="8" style={styles.td} ></td>
                            <td colSpan="2" style={styles.td} className=" bold text-right">Purchase Amount</td>
                            <td colSpan="3" style={styles.td} className="text-right"><span>Rs. {purchase.purchaseTotal}</span></td>
                          </tr>
                          <tr>
                            <td colSpan="4" style={styles.td}></td>
                            <td className=" bold text-right" style={styles.td}>VAT</td>
                            <td className="text-right" colSpan="2" style={styles.td}><b>5% :</b> Rs. {purchase.vat.five}</td>
                            <td className="text-right" colSpan="2" style={styles.td}><b>14% :</b> Rs. {purchase.vat.fourteen}</td>
                            <td className="text-right" colSpan="2" style={styles.td}><b>Exempted :</b> Rs. {purchase.vat.exempted}</td>
                            <td className="text-right" style={styles.td}>Rs. {purchase.vat.total}</td>
                          </tr>
                          <tr>
                            <td colSpan="10" style={styles.td}></td>
                            <td className=" bold text-right" style={styles.td}>Total</td>
                            <td className="text-right" style={styles.td}>Rs. {purchase.paymentAmount}</td>
                          </tr>
                      </tbody>
                  </table>*/}
                </div>
                :
                <div className="col s12">
                  <blockquote>
                    Please add products to purchase
                  </blockquote>
                </div>
              }
            </div>
          <div className="row">
             <div className="col offset-s10">
               <button onClick={this.savePurchase} type="button" className="waves-effect waves-light blue btn">Save(3)</button>
             </div>
         </div>
      </div>
      <Dialog
        title={newPurchaseProduct.name}
        actions={actions}
        modal={false}
        open={state.productsModalOpen}
        onRequestClose={this.closeProductsModal}
        autoScrollBodyContent={true}
        >
          <div>
              <duv className="row">
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        autoFocus={true}
                        floatingLabelText="Batch No"
                        style={styles.txtBox}
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.batchNo}
                        onChange={this.handleNewProductChange.bind(this, 'batchNo')} />
                  </div>
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="Exp Date"
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.expiryDate}
                        onChange={this.handleNewProductChange.bind(this, 'expiryDate')} />
                  </div>
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="Quantity "
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.stock}
                        onChange={this.handleNewProductChange.bind(this, 'stock')} />
                  </div>
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="Free"
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.freeUnit}
                        onChange={this.handleNewProductChange.bind(this, 'freeUnit')} />
                  </div>
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="Disc %"
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.discountPercentage}
                        onChange={this.handleNewProductChange.bind(this, 'discountPercentage')} />
                  </div>
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="P Rate"
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.purchaseRate}
                        onChange={this.handleNewProductChange.bind(this, 'purchaseRate')} />
                  </div>
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="MRP"
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.mrp}
                        onChange={this.handleNewProductChange.bind(this, 'mrp')} />
                  </div>
                  <div className="col 3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="VAT %"
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.vat}
                        onChange={this.handleNewProductChange.bind(this, 'vat')} />
                  </div>
                  <div className="col s3">
                      <TextField
                        onKeyDown = {this.checkEnterKeyPress.bind(this)}
                        style={styles.txtBox}
                        floatingLabelText="S. Rate"
                        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                        value={newPurchaseProduct.sellingRate}
                        onChange={this.handleNewProductChange.bind(this, 'sellingRate')} />
                  </div>
                  <div className="col s3">
                    <TextField
                      style={styles.txtBox}
                      floatingLabelText="Total"
                      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                      value={newPurchaseProduct.productTotal}
                      />
                  </div>
              </duv>
          </div>
      </Dialog>
      <Snackbar
          open={state.statusMessageOpen}
          message={state.statusMessage}
          autoHideDuration={4000}
          onRequestClose={this.statusMessageClose}
        />
    </div>

      )
  }
}
