import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon'

/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
 const downArrowStyles = {'marginTop':'-1px', 'marginLeft':'-1px'}
 export default class Appbar extends React.Component {

   constructor(props) {
     super(props);
     this.state = {
        open: false,
        adminMenuOpen: false,
        reportMenuOpen: false

      };

     this.handleAdminMenuOpen = this.handleAdminMenuOpen.bind(this)
     this.handleReportMenuOpen = this.handleReportMenuOpen.bind(this)
     this.handleRequestClose = this.handleRequestClose.bind(this)
   }

   isActive(tab){
     return this.props.activeTab == tab
   }

   handleAdminMenuOpen(event){
       console.log('hi')
        event.preventDefault();

        this.setState({
          adminMenuOpen: true,
          anchorEl: event.currentTarget,
        });
    }
    handleReportMenuOpen(event){
        console.log('hi')
         event.preventDefault();

         this.setState({
           reportMenuOpen: true,
           anchorEl: event.currentTarget,
         });
     }

    handleRequestClose(){
      this.setState({
        adminMenuOpen: false,
        reportMenuOpen: false
      });
    }


   render(){
      var state = this.state;
      return (
        <div className="navbar-fixed">
           <nav className="nav-extended light-blue darken-2">
              <div className="nav-wrapper">
                 <a href="#" className="brand-logo">Suguna Medicals</a>
                 <ul id="nav-mobile" className="right ">

                    <li>
                       <a href="#/" id="home_link" className = {this.isActive('dashboard')? 'active': ''}>
                         HOME
                       </a>
                    </li>
                    <li>
                      <a className="" onClick={this.handleAdminMenuOpen.bind(this)}>
                        ADMIN <i className="material-icons right" style={downArrowStyles}>arrow_drop_down</i>
                      </a>
                        <Popover
                          open={state.adminMenuOpen}
                          anchorEl={state.anchorEl}
                          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                          targetOrigin={{horizontal: 'left', vertical: 'top'}}
                          onRequestClose={this.handleRequestClose}
                        >
                          <Menu>
                            <MenuItem><a href="#purchase_entry">Purchase Entry</a></MenuItem>
                            <MenuItem><a href="#sales_entry">Sales Entry</a></MenuItem>
                            <MenuItem><a href="#product_entry">Product Entry</a></MenuItem>
                            <MenuItem><a href="#/vendor_entry">Vendor Entry</a></MenuItem>
                            <MenuItem><a href="#/salesman_entry">Sales Man Entry</a></MenuItem>
                          </Menu>
                        </Popover>
                    </li>
                    <li>
                      <a className="" onClick={this.handleReportMenuOpen.bind(this)}>
                        REPORTS <i className="material-icons right" style={downArrowStyles}>arrow_drop_down</i>
                      </a>
                        <Popover
                          open={state.reportMenuOpen}
                          anchorEl={state.anchorEl}
                          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                          targetOrigin={{horizontal: 'left', vertical: 'top'}}
                          onRequestClose={this.handleRequestClose}
                        >
                          <Menu>
                            <MenuItem><a href="#billwise_report">Bill Wise Report</a></MenuItem>
                            <MenuItem><a href="#itemwise_report">Item Wise Report</a></MenuItem>
                            <MenuItem><a href="#purchase_payment_report">Purchase Payment Report</a></MenuItem>
                          </Menu>
                        </Popover>
                    </li>
                    <li>
                       <a href="#/about-us" id="about_us_link" >
                         ABOUT US
                       </a>
                    </li>
                    <li>
                       <a href="#/contact" id="contact_us_link" >
                         CONTACT
                       </a>
                    </li>
                 </ul>
              </div>
           </nav>
        </div>
      )
  }
}
