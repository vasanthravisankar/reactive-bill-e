import React, { Component } from 'react'
import { Router, Route, Link, IndexRoute, hashHistory, browserHistory } from 'react-router'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Home from './components/home'
import ProductEntry from './components/product_entry'
import VendorEntry from './components/vendor_entry'
import SalesManEntry from './components/sales_man_entry'
import PurchaseEntry from './components/purchase_entry'
import SalesEntry from './components/sales_entry'




class App extends Component {
  constructor(props) {
    super(props);
    this.state = {open: false, activeTab: 'dashboard'};
  }
  render() {
    var state = this.state;
    var activeTab = state.activeTab
    return (
      <MuiThemeProvider>
      <Router history={hashHistory}>
          <Route path='/' component={Home} activeTab = 'dashboard'/>
          <Route path='/product_entry' component={ProductEntry} activeTab = 'product_entry' />
          <Route path='/vendor_entry' component={VendorEntry} activeTab = 'vendor_entry' />
          <Route path='/salesman_entry' component={SalesManEntry} activeTab = 'salesman_entry' />
          <Route path='/purchase_entry' component={PurchaseEntry} activeTab = 'purchase_entry' />
          <Route path='/sales_entry' component={SalesEntry} activeTab = 'sales_entry' />
          <Route path='*' component={NotFound} />
      </Router>
      </MuiThemeProvider>
    )
  }
}

const Address = () => <h1>We are located at 555 Jackson St.</h1>
const NotFound = () => (
  <h1>404.. This page is not found!</h1>)

  const Nav = () => (
    <div>
      <Link to='/'>Home</Link>&nbsp;
      <Link to='/address'>Address</Link>
    </div>
  )

  const Container = (props) => (<div>
      <Nav />
      {props.children}
    </div>
)
export default App
